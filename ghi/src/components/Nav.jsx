import useToken from '@galvanize-inc/jwtdown-for-react'
import { NavLink } from 'react-router-dom'
import '../css/Logo.css'

function MyNavBar() {
    const { token } = useToken()
    const { logout } = useToken()

    return (
        <nav
            className="navbar navbar-expand-lg"
            style={{ backgroundColor: '#e3f2fd' }}
        >
            <div className="container-fluid">
                <div className="logo">
                    <div className="letter-L"></div>
                    <div className="arrow"></div>
                    <div className="vertical-bar-2"></div>
                </div>
                <NavLink className="navbar-brand ms-5" to="/">
                    Level-Up Fitness
                </NavLink>
                <button
                    className="navbar-toggler"
                    type="button"
                    data-bs-toggle="collapse"
                    data-bs-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent"
                    aria-expanded="false"
                    aria-label="Toggle navigation"
                >
                    <span className="navbar-toggler-icon"></span>
                </button>
                {token ? (
                    <div
                        className="collapse navbar-collapse"
                        id="navbarSupportedContent"
                    >
                        <ul className="navbar-nav nav-pills me-auto mb-2 mb-lg-0">
                            <li className="nav-item">
                                <NavLink
                                    className="nav-link active"
                                    aria-current="page"
                                    to="userhome"
                                >
                                    Dashboard
                                </NavLink>
                            </li>
                            <li className="nav-item ms-3">
                                <NavLink
                                    className="nav-link active"
                                    aria-current="page"
                                    to="healthdata"
                                >
                                    Health Data
                                </NavLink>
                            </li>
                            <li className="nav-item ms-3">
                                <NavLink
                                    className="nav-link active"
                                    onClick={logout}
                                    aria-current="page"
                                    to="/"
                                >
                                    Logout
                                </NavLink>
                            </li>
                        </ul>
                    </div>
                ) : null}
            </div>
        </nav>
    )
}
export default MyNavBar
