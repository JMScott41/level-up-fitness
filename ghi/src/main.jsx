import React from 'react'
import ReactDOM from 'react-dom/client'
import { Provider } from 'react-redux'
import HomePage from './routes/HomePage.jsx'
import LoggedInHomePage from './routes/LoggedInHome.jsx'
import HealthData from './routes/HealthData.jsx'
import CreateHealthData from './routes/CreateHealthData.jsx'
import WorkoutPage from './routes/WorkoutPage.jsx'
import { createBrowserRouter, RouterProvider } from 'react-router-dom'
import Root from './routes/Root.jsx'

const router = createBrowserRouter([
    {
        element: <Root />,
        children: [
            {
                path: '/',
                element: <HomePage />,
            },
            {
                path: 'userhome',
                element: <LoggedInHomePage />,
            },
            {
                path: 'workout_page',
                element: <WorkoutPage />,
            },
            {
                path: 'healthdata',
                element: <HealthData />,
            },
            {
                path: 'healthdata/new',
                element: <CreateHealthData />,
            },
        ],
    },
])

ReactDOM.createRoot(document.getElementById('root')).render(
    <React.StrictMode>
        <RouterProvider router={router} />
    </React.StrictMode>
)
