import { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import '../css/HealthData.css'

const API_HOST = import.meta.env.VITE_API_HOST_FROM_ENV

if (!API_HOST) {
    throw new Error('VITE_API_HOST is not defined')
}

export default function CreateHealthData() {
    const navigate = useNavigate()

    const [height, setHeight] = useState('')
    const [weight, setWeight] = useState('')
    const [age, setAge] = useState('')
    const [level, setLevel] = useState('')
    const [goal, setGoal] = useState('')
    const [healthDataDate, setHealthDataDate] = useState('')

    const getUserData = async () => {
        const url = `${API_HOST}/token`
        const response = await fetch(url, {
            credentials: 'include',
        })
        if (response.ok) {
            const data = await response.json()
            setHealthDataDate(getDate())
            if (!data) {
                navigate('/')
            }
        }
    }

    function getDate() {
        const today = new Date()
        const year = today.getFullYear()
        const month = today.getMonth() + 1
        const paddedMonth = String(month).padStart(2, '0')
        const day = today.getDate()
        const paddedDay = String(day).padStart(2, '0')
        const currentDate = `${year}-${paddedMonth}-${paddedDay}`
        return currentDate
    }

    const handleCreateHealthdataSubmit = async (e) => {
        e.preventDefault()
        const form = e.currentTarget
        const healthData = {
            height: height,
            weight: weight,
            age: age,
            level: level,
            goal: goal,
            health_data_date: healthDataDate,
        }

        const locationURL = `${API_HOST}/api/health_data`

        const fetchConfig = {
            method: 'post',
            credentials: 'include',
            body: JSON.stringify(healthData),
            headers: {
                'Content-Type': 'application/json',
            },
        }

        const response = await fetch(locationURL, fetchConfig)

        if (response.ok) {
            form.reset()
            navigate('/healthdata')
        }
    }

    const fitnessLevel = ['Beginner', 'Intermediate', 'Advanced']

    useEffect(() => {
        getUserData()
    }, [])

    return (
        <div>
            <label className="fs-1 fw-bold title">
                Set your current health information
            </label>
            <p className="subtitle">Filling out data for {healthDataDate}</p>
            <div className="card mx-auto" style={{ width: '60rem' }}>
                <form
                    onSubmit={(e) => handleCreateHealthdataSubmit(e)}
                    id="create-healthdata-form"
                >
                    <div className="create-healthdata-form form-spacing">
                        <input
                            onChange={(e) => setHeight(e.target.value)}
                            placeholder="height"
                            required
                            type="number"
                            className="form-control"
                        />
                        <label htmlFor="starts">Your Height in Inches</label>
                    </div>
                    <div className="create-healthdata-form form-spacing">
                        <input
                            onChange={(e) => setWeight(e.target.value)}
                            placeholder="weight"
                            required
                            type="number"
                            className="form-control"
                        />
                        <label htmlFor="starts">Your Weight in Pounds</label>
                    </div>
                    <div className="create-healthdata-form form-spacing">
                        <input
                            onChange={(e) => setAge(e.target.value)}
                            placeholder="age"
                            required
                            type="number"
                            className="form-control"
                        />
                        <label htmlFor="starts">Your Age</label>
                    </div>

                    <div className="create-healthdata-form form-spacing">
                        <input
                            onChange={(e) => setGoal(e.target.value)}
                            placeholder="What's your fitness goal?"
                            required
                            type="text"
                            className="form-control"
                        />
                        <label htmlFor="starts">Your Fitness Goal</label>
                    </div>

                    <div className="create-healthdata-form form-spacing">
                        <select
                            name="level"
                            id="level"
                            onChange={(e) => setLevel(e.target.value)}
                            value={level}
                            className="form-select"
                        >
                            <option
                                className="workout-page-btn span-btn"
                                value=""
                            >
                                Fitness Level
                            </option>
                            {fitnessLevel.map((level, index) => {
                                return (
                                    <option
                                        onClick={(level) => setLevel(level)}
                                        key={index}
                                        value={level}
                                    >
                                        {level}
                                    </option>
                                )
                            })}
                        </select>
                        <label htmlFor="starts">
                            Your Current Fitness Level
                        </label>
                    </div>
                    <button className="btn health-button button-pad">
                        Submit
                    </button>
                </form>
            </div>
        </div>
    )
}
