import { useState, useEffect } from 'react'
import { useNavigate } from 'react-router-dom'
import { useAuthContext } from '@galvanize-inc/jwtdown-for-react'
import { login, signup } from '../services/auth'
import '../css/HomePage.css'

const API_HOST = import.meta.env.VITE_API_HOST_FROM_ENV

if (!API_HOST) {
    throw new Error('VITE_API_HOST is not defined')
}

function HomePage() {
    const [signupErrorMessage, setSignupErrorMessage] = useState('')
    const [loginErrorMessage, setLoginErrorMessage] = useState('')
    const [passwordErrorMessage, setPasswordErrorMessage] = useState('')
    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')
    const [email, setEmail] = useState('')
    const [verifiedPassword, setVerifiedPassword] = useState('')
    const [loginUsername, setLoginUsername] = useState('')
    const [loginPassword, setLoginPassword] = useState('')
    const { setToken, baseUrl } = useAuthContext()
    const navigate = useNavigate()

    const handleSignupSubmit = async (e) => {
        e.preventDefault()
        const form = e.currentTarget
        const accountData = {
            username: username,
            email: email,
            password: password,
        }
        if (password === verifiedPassword) {
            try {
                await signup(accountData)
                const token = await login(
                    baseUrl,
                    accountData.username,
                    accountData.password
                )
                setToken(token)
                form.reset()
                navigate('userhome')
            } catch (e) {
                if (e instanceof Error) {
                    setSignupErrorMessage(e.message)
                }
            }
        } else {
            setPasswordErrorMessage('Passwords do not match')
        }
    }

    const handleLoginSubmit = async (e) => {
        e.preventDefault()
        const form = e.currentTarget
        try {
            const token = await login(baseUrl, loginUsername, loginPassword)
            setToken(token)
            form.reset()
            navigate('userhome')
        } catch (e) {
            if (e instanceof Error) {
                setLoginErrorMessage(e.message)
            }
        }
    }

    return (
        <>
            <div
                id="carouselExampleIndicators"
                className="carousel slide"
                data-bs-ride="carousel"
            >
                <div className="carousel-indicators">
                    <button
                        type="button"
                        data-bs-target="#carouselExampleIndicators"
                        data-bs-slide-to="0"
                        className="active"
                        aria-current="true"
                        aria-label="Slide 1"
                    ></button>
                    <button
                        type="button"
                        data-bs-target="#carouselExampleIndicators"
                        data-bs-slide-to="1"
                        aria-label="Slide 2"
                    ></button>
                    <button
                        type="button"
                        data-bs-target="#carouselExampleIndicators"
                        data-bs-slide-to="2"
                        aria-label="Slide 3"
                    ></button>
                </div>
                <div className="carousel-inner">
                    <div className="carousel-item active">
                        <img
                            src="https://www.messagemagazine.com/wp-content/uploads/2019/08/CrossFit-Workouts-5-WODs-1.jpg"
                            className="d-block w-100"
                            alt="..."
                        />
                    </div>
                    <div className="carousel-item">
                        <img
                            src="https://dayspaassociation.com/wp-content/uploads/2022/08/pexels-cesar-galeao-3253501.jpg"
                            className="d-block w-100"
                            alt="..."
                        />
                    </div>
                    <div className="carousel-item">
                        <img
                            src="https://hips.hearstapps.com/hmg-prod/images/four-people-doing-box-jumps-in-cross-training-class-royalty-free-image-1651067413.jpg"
                            className="d-block w-100"
                            alt="..."
                        />
                    </div>
                </div>
                <button
                    className="carousel-control-prev"
                    type="button"
                    data-bs-target="#carouselExampleIndicators"
                    data-bs-slide="prev"
                >
                    <span
                        className="carousel-control-prev-icon"
                        aria-hidden="true"
                    ></span>
                    <span className="visually-hidden">Previous</span>
                </button>
                <button
                    className="carousel-control-next"
                    type="button"
                    data-bs-target="#carouselExampleIndicators"
                    data-bs-slide="next"
                >
                    <span
                        className="carousel-control-next-icon"
                        aria-hidden="true"
                    ></span>
                    <span className="visually-hidden">Next</span>
                </button>
            </div>
            <div>
                <h1 className="fw-light ml mt-5 pt-5 text-decoration-underline title">
                    Level Up Fitness
                </h1>
                <div className="ml pl">
                    <img src="Level-Up-Logo.png" />
                </div>
            </div>
            <div className="card-container">
                <div className="card">
                    <img
                        src="https://www.freebiefindingmom.com/wp-content/uploads/2022/01/printable-inspiring-fitness-motivation-quotes.jpg"
                        className="card-img-top"
                        alt="..."
                    />
                    <div className="card-body">
                        <h5 className="card-title home-page-btn text-center">
                            Your New Habit is Here
                        </h5>
                        <p className="card-text fw-bold text-center">
                            MOTIVATION is what gets you started. HABIT is what
                            you keeps you going.
                        </p>
                    </div>
                </div>
                <div>
                    <div className="main">
                        <input
                            className="signup-form-input"
                            type="checkbox"
                            id="chk"
                            aria-hidden="true"
                        />
                        <div className="signup">
                            <form
                                onSubmit={(e) => handleSignupSubmit(e)}
                                className="row"
                                id="signup-form"
                            >
                                <label
                                    className="signup-form-label"
                                    htmlFor="chk"
                                    aria-hidden="true"
                                >
                                    Sign up
                                </label>
                                {signupErrorMessage ? (
                                    <div
                                        className="position-relative alert alert-danger alert-dismissable error-message"
                                        role="alert"
                                    >
                                        <p>{signupErrorMessage}</p>
                                        <button
                                            type="button"
                                            className="close"
                                            data-dismiss="alert"
                                            aria-label="Close"
                                            onClick={(e) =>
                                                setSignupErrorMessage('')
                                            }
                                            style={{
                                                position: 'absolute',
                                                top: '0',
                                                right: '0',
                                                border: 'none',
                                                background: 'transparent',
                                            }}
                                        >
                                            <span aria-hidden="true">
                                                &times;
                                            </span>
                                        </button>
                                    </div>
                                ) : null}
                                {passwordErrorMessage ? (
                                    <div
                                        className="position-relative alert alert-danger alert-dismissable error-message"
                                        role="alert"
                                    >
                                        <p>{passwordErrorMessage}</p>
                                        <button
                                            type="button"
                                            className="close"
                                            data-dismiss="alert"
                                            aria-label="Close"
                                            onClick={(e) =>
                                                setPasswordErrorMessage('')
                                            }
                                            style={{
                                                position: 'absolute',
                                                top: '0',
                                                right: '0',
                                                border: 'none',
                                                background: 'transparent',
                                            }}
                                        >
                                            <span aria-hidden="true">
                                                &times;
                                            </span>
                                        </button>
                                    </div>
                                ) : null}
                                <div className="col-auto">
                                    <input
                                        name="username"
                                        type="text"
                                        className="form-control"
                                        placeholder="Username"
                                        aria-label="Username"
                                        onChange={(e) =>
                                            setUsername(e.target.value)
                                        }
                                    />
                                </div>
                                <div className="col-auto">
                                    <input
                                        name="email"
                                        type="text"
                                        className="form-control"
                                        placeholder="Email"
                                        aria-label="Email"
                                        onChange={(e) =>
                                            setEmail(e.target.value)
                                        }
                                    />
                                </div>
                                <div className="col-auto">
                                    <input
                                        name="password"
                                        type="password"
                                        className="form-control"
                                        id="exampleInputPassword1"
                                        placeholder="Password"
                                        onChange={(e) =>
                                            setPassword(e.target.value)
                                        }
                                    />
                                </div>
                                <div className="col-auto">
                                    <input
                                        name="verifiedPassword"
                                        type="password"
                                        className="form-control"
                                        id="exampleInputPasswordConfirmation1"
                                        placeholder="Confirm Password"
                                        onChange={(e) =>
                                            setVerifiedPassword(e.target.value)
                                        }
                                    />
                                </div>
                                <button
                                    className="signup-form-button"
                                    type="submit"
                                    value="Register"
                                >
                                    Sign up
                                </button>
                            </form>
                        </div>
                        <div className="login">
                            <form
                                onSubmit={(event) => handleLoginSubmit(event)}
                                className="login-container"
                                id="login-form"
                            >
                                <label
                                    className="signup-form-label"
                                    htmlFor="chk"
                                    aria-hidden="true"
                                >
                                    Login
                                </label>
                                {loginErrorMessage ? (
                                    <div
                                        className="position-relative alert alert-danger alert-dismissable error-message"
                                        role="alert"
                                    >
                                        <p>{loginErrorMessage}</p>
                                        <button
                                            type="button"
                                            className="close"
                                            data-dismiss="alert"
                                            aria-label="Close"
                                            onClick={(e) =>
                                                setLoginErrorMessage('')
                                            }
                                            style={{
                                                position: 'absolute',
                                                top: '0',
                                                right: '0',
                                                border: 'none',
                                                background: 'transparent',
                                            }}
                                        >
                                            <span aria-hidden="true">
                                                &times;
                                            </span>
                                        </button>
                                    </div>
                                ) : null}
                                <div className="">
                                    <input
                                        name="loginUsername"
                                        type="text"
                                        className="form-control"
                                        placeholder="Username"
                                        onChange={(event) =>
                                            setLoginUsername(event.target.value)
                                        }
                                    />
                                </div>
                                <div>
                                    <input
                                        name="loginPassword"
                                        type="password"
                                        className="form-control"
                                        placeholder="Password"
                                        onChange={(event) =>
                                            setLoginPassword(event.target.value)
                                        }
                                    />
                                </div>
                                <button
                                    type="submit"
                                    className="signup-form-button"
                                    value="Login"
                                >
                                    Login
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
                <div className="card">
                    <img
                        src="https://www.freebiefindingmom.com/wp-content/uploads/2022/01/printable-inspiring-fitness-motivation-quotes.jpg"
                        className="card-img-top"
                        alt="..."
                    />
                    <div className="card-body">
                        <h5 className="card-title home-page-btn text-center">
                            Pain is Temporary
                        </h5>
                        <p className="card-text fw-bold text-center">
                            The pain you feel today is the strength you feel
                            tomorrow.
                        </p>
                    </div>
                </div>
            </div>
        </>
    )
}

export default HomePage
