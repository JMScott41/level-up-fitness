import { useState, useEffect } from 'react'
import FullCalendar from '@fullcalendar/react'
import dayGridPlugin from '@fullcalendar/daygrid'
import interactionPlugin from '@fullcalendar/interaction'
import { useNavigate } from 'react-router-dom'
import { useAuthContext } from '@galvanize-inc/jwtdown-for-react'
import '../css/LoggedInHome.css'
import { NavLink } from 'react-router-dom'

const API_HOST = import.meta.env.VITE_API_HOST_FROM_ENV

if (!API_HOST) {
    throw new Error('VITE_API_HOST is not defined')
}

export default function LoggedInHomePage() {
    const navigate = useNavigate()
    const { token } = useAuthContext()
    const [clickedDate, setClickedDate] = useState()
    const [workoutList, setWorkoutList] = useState([])
    const [healthData, setHealthData] = useState([])
    const [workoutDate, setWorkoutDate] = useState(
        new Date().toISOString().split('T')[0]
    )

    const fetchWorkout = async (selectedDate) => {
        try {
            const response = await fetch(
                `${API_HOST}/api/workouts/${selectedDate}`,
                {
                    headers: { Authorization: `Bearer ${token}` },
                }
            )

            if (!response.ok) {
                throw new Error('Failed to fetch workout data')
            }
            const data = await response.json()
            setWorkoutList(data)
        } catch (error) {
            console.error('Error fetching workout data:', error)
        }
    }

    const fetchHealthData = async () => {
        try {
            const response = await fetch(`${API_HOST}/api/health_data`, {
                headers: { Authorization: `Bearer ${token}` },
            })

            if (!response.ok) {
                throw new Error('Failed to fetch workout data')
            }
            const data = await response.json()
            setHealthData(data)
        } catch (error) {
            console.error('Error fetching Health data:', error)
        }
    }

    useEffect(() => {
        if (token) {
            const today = new Date()
            const formattedDate = today.toISOString().split('T')[0]
            fetchWorkout(formattedDate)
            setClickedDate('Today')
            fetchHealthData()
        } else {
            navigate('/')
        }
    }, [token])

    const handleDateClick = (date) => {
        const selectedDate = date.dateStr
        setWorkoutDate(selectedDate)

        const options = {
            weekday: 'long',
            year: 'numeric',
            month: 'long',
            day: 'numeric',
        }
        const headerDate = new Date(date.dateStr)
        const formattedHeaderDate = headerDate.toLocaleDateString(
            undefined,
            options
        )
        fetchWorkout(selectedDate)
        setClickedDate(formattedHeaderDate)
    }

    const navi = () => {
        navigate('/workout_page', {
            state: {
                date: workoutDate,
            },
        })
    }

    return (
        <>
            <div className="main-logged-in">
                <div className="calendar">
                    <h1 className="fs-1 ml">
                        Choose a Date to See Your Workout..
                    </h1>
                    <section>
                        <FullCalendar
                            plugins={[dayGridPlugin, interactionPlugin]}
                            dateClick={handleDateClick}
                            initialView="dayGridMonth"
                            selectable={true}
                        />
                    </section>
                </div>
                <div className="health-data-table">
                    <h1 className="fs-1 ml">Your Health Data</h1>
                    <table>
                        <thead>
                            <tr>
                                <th>Weight</th>
                                <th>Goal</th>
                                <th>Last Updated</th>
                            </tr>
                        </thead>
                        <tbody>
                            {healthData
                                .slice()
                                .reverse()
                                .slice(0, 10)
                                .map((health) => (
                                    <tr key={health.id}>
                                        <td>{health.weight}</td>
                                        <td>{health.goal}</td>
                                        <td>{health.health_data_date}</td>
                                    </tr>
                                ))}
                        </tbody>
                    </table>
                    <div className="button-container">
                        <NavLink to="/healthdata">
                            <button className="btn workout-page-button health-data-button">
                                To Health Data Main Page
                            </button>
                        </NavLink>
                    </div>
                </div>
                <div className="workout-table">
                    <h1 className="fs-1 ml">
                        Workout Completed For {clickedDate}
                    </h1>
                    <div>
                        {workoutList.length === 0 ? (
                            <p>No workouts for that date. Let's get started!</p>
                        ) : (
                            <table>
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Sets</th>
                                        <th>Reps</th>
                                        <th>Time</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {workoutList.map((exercise) => (
                                        <tr key={exercise.id}>
                                            <td>{exercise.name}</td>
                                            <td>
                                                {exercise.sets
                                                    ? exercise.sets
                                                    : 'N/A'}
                                            </td>
                                            <td>
                                                {exercise.reps
                                                    ? exercise.reps
                                                    : 'N/A'}
                                            </td>
                                            <td>
                                                {exercise.time
                                                    ? exercise.time
                                                    : 'N/A'}
                                            </td>
                                        </tr>
                                    ))}
                                </tbody>
                            </table>
                        )}
                    </div>
                    <div onClick={navi} className="button-container">
                        <button className="btn workout-page-button">
                            To This Day's Workout
                        </button>
                    </div>
                </div>
            </div>
        </>
    )
}
