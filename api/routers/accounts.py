from fastapi import (
    APIRouter,
    Request,
    Response,
    Depends,
    HTTPException,
)
from auth_models import AccountToken, AccountIn, AccountForm, HttpError
from queries.accounts import AccountsQueries
from authenticator import authenticator
import psycopg


router = APIRouter()


@router.post("/api/accounts", response_model=AccountToken | HttpError)
async def create_account(
    user: AccountIn,
    request: Request,
    response: Response,
    accounts: AccountsQueries = Depends(),
):
    hashed_password = authenticator.hash_password(user.password)
    try:
        account = accounts.create(user, hashed_password)
    except psycopg.errors.UniqueViolation as e:
        raise e
    form = AccountForm(username=user.username, password=user.password)
    token = await authenticator.login(response, request, form, accounts)
    return AccountToken(account=account, **token.dict())


@router.get("/token", response_model=AccountToken | None)
async def get_token(
    request: Request,
    account: dict = Depends(authenticator.try_get_current_account_data),
) -> AccountToken | HTTPException:
    if account and authenticator.cookie_name in request.cookies:
        return {
            "access_token": request.cookies[authenticator.cookie_name],
            "type": "Bearer",
            "account": account,
        }
