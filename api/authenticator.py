# authenticator.py
import os
from fastapi import Depends
from jwtdown_fastapi.authentication import Authenticator
from queries.accounts import AccountsQueries
from auth_models import AccountOutWithHashedPassword


class LevelUpAuthenticator(Authenticator):
    async def get_account_data(
        self,
        username: str,
        accounts: AccountsQueries,
    ):
        return accounts.get(username)

    def get_account_getter(
        self,
        accounts: AccountsQueries = Depends(),
    ):
        return accounts

    def get_hashed_password(self, account: AccountOutWithHashedPassword):
        return account.hashed_password

    def get_account_data_for_cookie(self, account):
        if isinstance(account, dict):
            account = AccountOutWithHashedPassword(**account)
        return account.username, account.dict()


SIGNING_KEY = os.environ.get("SIGNING_KEY")
if not SIGNING_KEY:
    raise ValueError("SIGNING_KEY environment variable not defined")
authenticator = LevelUpAuthenticator(SIGNING_KEY)
