from pydantic import BaseModel


class thirdPartyExerciseItem(BaseModel):
    name: str
    type: str
    muscle: str
    equipment: str
    difficulty: str
    instructions: str
