steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE users (
            id SERIAL PRIMARY KEY NOT NULL,
            username VARCHAR(150) NOT NULL UNIQUE,
            email VARCHAR(150) NOT NULL,
            hashed_password VARCHAR(150) NOT NULL
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE users;
        """,
    ],
    [
        """
        CREATE TABLE exercises (
            id SERIAL PRIMARY KEY,
            user_id INT NOT NULL REFERENCES users(id),
            name VARCHAR(100) NOT NULL,
            type VARCHAR(100) NOT NULL,
            muscle VARCHAR(100) NOT NULL,
            equipment VARCHAR(100) NOT NULL,
            difficulty VARCHAR(100) NOT NULL,
            instructions TEXT,
            sets INT NULL,
            reps INT NULL,
            time INT NULL,
            exercise_date DATE NOT NULL DEFAULT CURRENT_DATE
        );
        """,
        """
        DROP TABLE exercises;
        """,
    ],
    [
        """
        CREATE TABLE health_data (
            id SERIAL PRIMARY KEY,
            user_id INT NOT NULL REFERENCES users(id),
            height INT NOT NULL,
            weight INT NULL,
            age INT NULL,
            level VARCHAR(100) NOT NULL,
            goal VARCHAR(100) NOT NULL,
            health_data_date DATE NOT NULL DEFAULT CURRENT_DATE
        );
        """,
        """
        DROP TABLE health_data;
        """,
    ],
]
