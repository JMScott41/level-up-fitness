import requests
import os
from models.thirdp_exercise_models import thirdPartyExerciseItem
from typing import List


API_NINJAS_API_KEY = os.environ["API_NINJAS_API_KEY"]


class thirdPartyExerciseRepository:
    def get_exercises(
        self, muscle: str
    ) -> List[thirdPartyExerciseItem] | None:
        api_url = "https://api.api-ninjas.com/v1/exercises?muscle={}".format(
            muscle
        )
        response = requests.get(
            api_url, headers={"X-Api-Key": API_NINJAS_API_KEY}
        )
        if response.ok:
            data = response.json()
            return data
        else:
            print("Error:", response.status_code, response.text)
