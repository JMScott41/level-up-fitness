from models.exercise_models import ExerciseIn, ExerciseOut
from queries.pool import pool
from typing import List
from datetime import date


class CreateExerciseError(ValueError):
    pass


class GetAllExercisesError(ValueError):
    pass


class GetWorkoutError(ValueError):
    pass


class GetOneExerciseError(ValueError):
    pass


class UpdateExerciseError(ValueError):
    pass


class ExerciseRepository:
    def exercise_out(self, exercise):
        return ExerciseOut(
            id=exercise[0],
            user_id=exercise[1],
            name=exercise[2],
            type=exercise[3],
            muscle=exercise[4],
            equipment=exercise[5],
            difficulty=exercise[6],
            instructions=exercise[7],
            sets=exercise[8],
            reps=exercise[9],
            time=exercise[10],
            exercise_date=exercise[11],
        )

    def get_all(self, user_id: int) -> List[ExerciseOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT *
                        FROM exercises
                        WHERE user_id = %s
                        ORDER BY id;
                        """,
                        [user_id],
                    )
                    exercises = result.fetchall()
                    exercise_list = []
                    for exercise in exercises:
                        exercise_list.append(self.exercise_out(exercise))
                    return exercise_list
        except Exception as e:
            print(e)
            raise GetAllExercisesError("Could not update exercise")

    def get_workout(
        self, user_id: int, exercise_date: date
    ) -> List[ExerciseOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT *
                        FROM exercises
                        WHERE user_id = %s AND exercise_date = %s
                        """,
                        [user_id, exercise_date],
                    )
                    exercises = result.fetchall()
                    exercise_list = []
                    for exercise in exercises:
                        exercise_list.append(self.exercise_out(exercise))
                    return exercise_list
        except Exception as e:
            print(e)
            raise GetWorkoutError("Could not retrieve workout")

    def get_one(self, user_id: int, exercise_id: int) -> ExerciseOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT *
                        FROM exercises
                        WHERE user_id = %s AND id = %s
                        """,
                        [user_id, exercise_id],
                    )
                    record = result.fetchone()
                    return self.exercise_out(record)
        except Exception as e:
            print(e)
            raise GetOneExerciseError("Could not retrieve the exercise")

    def delete(self, user_id: int, exercise_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM exercises
                        WHERE user_id = %s AND id = %s
                        """,
                        [user_id, exercise_id],
                    )
                    return True
        except Exception:
            return False

    def update_exercise(
        self, user_id: int, exercise_id: int, exercise: ExerciseIn
    ) -> ExerciseOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE exercises
                        SET name = %s,
                            type = %s,
                            muscle = %s,
                            equipment = %s,
                            difficulty = %s,
                            instructions = %s,
                            sets = %s,
                            reps = %s,
                            time = %s,
                            exercise_date = %s
                        WHERE user_id = %s AND id = %s
                        RETURNING *;
                        """,
                        [
                            exercise.name,
                            exercise.type,
                            exercise.muscle,
                            exercise.equipment,
                            exercise.difficulty,
                            exercise.instructions,
                            exercise.sets,
                            exercise.reps,
                            exercise.time,
                            exercise.exercise_date,
                            user_id,
                            exercise_id,
                        ],
                    )
                    row = db.fetchone()
                    if row is not None:
                        record = {}
                        for i, column in enumerate(db.description):
                            record[column.name] = row[i]
                    return ExerciseOut(**record)

        except Exception:
            raise Exception

    def create(self, user_id: int, exercise: ExerciseIn) -> ExerciseOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    cur.execute(
                        """
                        INSERT INTO exercises
                            (
                                user_id,
                                name,
                                type,
                                muscle,
                                equipment,
                                difficulty,
                                instructions,
                                sets,
                                reps,
                                time,
                                exercise_date
                            )
                        VALUES
                            (
                                %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s
                            )
                        RETURNING *;
                        """,
                        [
                            user_id,
                            exercise.name,
                            exercise.type,
                            exercise.muscle,
                            exercise.equipment,
                            exercise.difficulty,
                            exercise.instructions,
                            exercise.sets,
                            exercise.reps,
                            exercise.time,
                            exercise.exercise_date,
                        ],
                    )
                    record = None
                    row = cur.fetchone()
                    if row is not None:
                        record = {}
                        for i, column in enumerate(cur.description):
                            record[column.name] = row[i]
                    return ExerciseOut(**record)
        except Exception as e:
            print(e)
            raise CreateExerciseError("Could not create exercise")
