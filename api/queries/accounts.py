from auth_models import AccountIn, AccountOutWithHashedPassword
from fastapi import HTTPException
from queries.pool import pool
import psycopg


class AccountsQueries:
    def get(self, username: str) -> AccountOutWithHashedPassword:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                try:
                    cur.execute(
                        """
                            SELECT *
                            FROM users
                            WHERE username = %s;
                            """,
                        [username],
                    )
                    record = None
                    for row in cur.fetchall():
                        record = {}
                        for i, column in enumerate(cur.description):
                            record[column.name] = row[i]
                        return AccountOutWithHashedPassword(**record)
                except Exception as e:
                    print(e)
                    raise HTTPException(
                        status_code=404, detail="Account not found"
                    )

    def create(
        self, user: AccountIn, hashed_password: str
    ) -> AccountOutWithHashedPassword:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                params = [user.username, user.email, hashed_password]
                try:
                    cur.execute(
                        """
                            INSERT INTO users
                                (username, email, hashed_password)
                            VALUES
                                (%s, %s, %s)
                            RETURNING id, username, email, hashed_password;
                            """,
                        params,
                    )
                    record = None
                    row = cur.fetchone()
                    if row is not None:
                        record = {}
                        for i, column in enumerate(cur.description):
                            record[column.name] = row[i]
                    return AccountOutWithHashedPassword(**record)
                except psycopg.errors.UniqueViolation:
                    raise HTTPException(
                        status_code=409,
                        detail="Conflict: Duplicate username or email",
                    )
                except Exception as e:
                    print(e)
                    raise HTTPException(
                        status_code=500, detail="Internal Server Error"
                    )
