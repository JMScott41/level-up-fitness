from typing import List
from queries.pool import pool
from models.health_data_models import HealthDataIn, HealthDataOut


class GetHealthDataError(ValueError):
    pass


class GetAllHealthDataError(ValueError):
    pass


class CreateHealthDataError(ValueError):
    pass


class HealthDataRepository:
    def health_data_out(self, health_data):
        return HealthDataOut(
            id=health_data[0],
            user_id=health_data[1],
            height=health_data[2],
            weight=health_data[3],
            age=health_data[4],
            level=health_data[5],
            goal=health_data[6],
            health_data_date=health_data[7],
        )

    def get(self, user_id: int, health_data_id: int) -> HealthDataOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    cur.execute(
                        """
                        SELECT *
                        FROM health_data
                        WHERE user_id = %s AND id = %s
                        """,
                        [
                            user_id,
                            health_data_id,
                        ],
                    )
                    record = None
                    for row in cur.fetchall():
                        record = {}
                        for i, column in enumerate(cur.description):
                            record[column.name] = row[i]
                    return HealthDataOut(**record)
        except Exception as e:
            print(e)
            raise GetHealthDataError("Could not get health data")

    def get_all(self, user_id: int) -> List[HealthDataOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT *
                        FROM health_data
                        WHERE user_id = %s
                        ORDER BY id;
                        """,
                        [user_id],
                    )
                    health_datas = result.fetchall()
                    health_data_list = []
                    for health_data in health_datas:
                        health_data_list.append(
                            self.health_data_out(health_data)
                        )
                    return health_data_list
        except Exception as e:
            print(e)
            raise GetAllHealthDataError("Could not retrieve health data")

    def create(self, user_id: int, health_data: HealthDataIn) -> HealthDataOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as cur:
                    cur.execute(
                        """
                        INSERT INTO health_data
                            (
                            user_id,
                            height,
                            weight,
                            age,
                            level,
                            goal,
                            health_data_date
                            )
                        VALUES
                            (%s, %s, %s, %s, %s, %s, %s)
                        RETURNING *;
                        """,
                        [
                            user_id,
                            health_data.height,
                            health_data.weight,
                            health_data.age,
                            health_data.level,
                            health_data.goal,
                            health_data.health_data_date,
                        ],
                    )
                    record = None
                    row = cur.fetchone()
                    if row is not None:
                        record = {}
                        for i, column in enumerate(cur.description):
                            record[column.name] = row[i]
                    return HealthDataOut(**record)
        except Exception as e:
            print(e)
            raise CreateHealthDataError("Unable to Create Health Data")
