from fastapi.testclient import TestClient
from main import app
from authenticator import authenticator
from queries.health_data import HealthDataRepository
from models.health_data_models import HealthDataOut


client = TestClient(app)


class MockHealthDataRepository:
    def create(self, user_id, health_data):
        return HealthDataOut(
            id=1,
            user_id=user_id,
            height=health_data.height,
            weight=health_data.weight,
            age=health_data.age,
            level=health_data.level,
            goal=health_data.goal,
            health_data_date=health_data.health_data_date,
        )


def mock_get_account_data():
    return {"id": 1, "username": "username", "email": "email@example.com"}


def test_create_healthdata():
    app.dependency_overrides[
        authenticator.try_get_current_account_data
    ] = mock_get_account_data

    app.dependency_overrides[HealthDataRepository] = MockHealthDataRepository

    healthdata_json = {
        "height": 0,
        "weight": 0,
        "age": 0,
        "level": "string",
        "goal": "string",
        "health_data_date": "2001-01-01",
    }

    response = client.post("/api/health_data", json=healthdata_json)

    app.dependency_overrides = {}

    expected_response = {
        "id": 1,
        "user_id": 1,
        "height": healthdata_json["height"],
        "weight": healthdata_json["weight"],
        "age": healthdata_json["age"],
        "level": healthdata_json["level"],
        "goal": healthdata_json["goal"],
        "health_data_date": healthdata_json["health_data_date"],
    }

    assert response.status_code == 200
    assert response.json() == expected_response
