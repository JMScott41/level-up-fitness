## January 16

Today I worked with my team to:

* Set up our CRUD database commands that we will use to POST, GET, PUT, and DELETE users from our database.

I also set up our docker-compose.yml file to initiate our postgreSQL container and tested out the functionality of pgadmin in database testing and implementation.

I realized that setting up the database and table was much simpler from within the terminal than with pgadmin. I think I will continue to use this method as we progress through the project. I would like to circle back to pgadmin later though if time permits some deeper exploration.

## January 17

Today I worked with my team to:

*Implement user account authentication with JWT and create our first backend FAST API request.

We decided to go through this segment together knowing that authentication would be difficult. Lucas drove while the rest of us reviewed the material from Learn and Riley's lecture.

However, we've hit a small block due to the fact that all of the examples we've been delivered have dealt with Mongo and we are using PostgreSQL. We were unable to solve our issues before the rooms were closed for the night, so we'll have some questions to present tomorrow.

## January 18

Today I worked with my team to:

*Finish the implementation of account authentication features.

We continued to work together on this feature. I 'drove' and received valuable input and suggestions from my teammates. We completed the authentication feature shortly after lunch because we did a majority of the work the previous day. We realized that there was an error in our queries.accounts.py file after reviewing the lecture that Dalonte gave on authentication.

After that Jon took the driving wheel to begin working on our exercise POST feature. We set up our database, migrations table, and exercise query and router files. We're running into some kind of validation issues during the submission on the FastAPI dashboard, so that will be our focus tomorrow.

## January 19

Today I worked with my team to:

* Complete all of the CRUD routes for our workout database
Hector and Jon took turns driving today. We were able to successfully implement our POST, GET, PUT, and DELETE routes for our exercises. It turns out that our primary (but not only) error from yesterday, is that we were saving our return value in a variable, but never actually returning it. We ran into a second issue later with our GET for returning all of our exercises because we were returning a single exercise when it should have been returning a list. After that we had pretty smooth sailing with the remainder of the routes. Next week we will focus on the 3rd party APIs and workout routes.

## January 22

Today I worked with my team to:

* Create a health data table with a foreign key to a user's account. Jon drove for this implementation. We resolved some minor issues regarding the syntax required to associate a user's health data with their account, but it turned out to be a simple solution. We also completed the post method to create the health data and decided not to implement an update method because we want to track the users progress over time, so when they "update" their progress, it will instead be another post method.

I took over the coding to implement the "get all" request for the user's health data. It was pretty straightforward, but we ran into a type error that turned out to be an oversight on my part. I forgot to include the individual paramaters from the health_data table in the return statement of the get request. But we all quickly resolved the issue and everything works very well.

## January 23

Today I worked with my team to:

* Finish our back end exercise requests to complete our user functionality features, standardize our code formats, and include better error handling in each of our backend routes.

We finished our backend routes ahead of schedule without any issues on the ones we worked on today. So we decided to take the extra time to improve our error handling and reformat our code to be more standardized for each of our routes. We needed a little explanation from Bart to understand the different types and methods of error handling, but we were able to successfully implement them for each route in the case of erroneous input.

## January 24

Today I worked with my team to:

* Start on the front end of our application and familiarize ourselves with Redux.

We decided to utilize Redux to manage our front end state and to use bootstrap for our styling. We began with Redux until we actually needed to begin putting actual content into the browser. We then focused on figuring out how to install bootstrap into our Vite application. The steps were a little hard to follow, but with some brainstorming we managed to populate a very nifty bootstrap navbar on our browser.

## January 25

Today I worked on:

* Implementing a 'create user unit test'

I started with pytest and ran into plenty of errors. I decided to implement the unittest module and the MagicMock function to mock my data. That solved some problems but created new ones. I spent all day on this and will have to pick it up tomorrow with some new perspective.

## January 26

Today I worked on:

* Implementing create user unit test, Lucas worked on Health data unit test

After lots of trial and error, I found a way to mimic data with a technique called 'monkeypatch' which again solved some problems and created new ones. After a long day of attempts we requested help from the SEIRs who told me that mocking all of the data for creating a user is too complicated and that I should focucs on a different, simpler unit test.

## January 29

Today I worked with my team to:

* Create our front end homepage for not logged in users

We worked together to set up the initial homepage for our website. We incorporated bootstrap elements likes cards and a carousel and a navbar. Jon built the signup and login forms from scratch and they look really good. Tomorrow we will work on the form functionality.

## January 30

Today I worked on:

* Frontend authentication and creating a user

Today I coded through frontend authentication with the help of the learn modules. We initially got plenty of errors that turned out to be a case of mis-capitalizing a required argument for our signup function. However, I was finally able to successfully create a user in the database from the front end.

## January 31

Today I worked with my team to:

* Implement the user login/logout functionality and get started on the remaining front end page designs.

Today I primarily focused on allowing the user to login and logout and dynamically showing certain navbar links based on their login status. I tried and failed with many attempts, but it turned out to be a far too simple solution. JWTdown has a built in useToken() function that handles the user token sent back during account creation and login that I wasn’t referencing properly at first, but once I realized my error I was able to succesfully implement the functionality.

## February 1

Today I worked with my team to:

* Implement better error handling and provide dynamic error messages to the user on the login and signup forms.

I spent a lot of time looking for documentation on psycopg errors because our most common problem was when a user attempted to signup with a username that was already taken. Because our database is set up to have a unique username, psycopg would throw an error (intentionally) when that username already existed. I finally found some resources and was able to successfully log the error. Then I provided dynamic messaging on the front end depending on whether or not there was an error by using the useState hook to update if an error showed up. I then allowed the user to close the error message by simply updating the error state to empty when they clicked the button.

## February 5

Today I worked with my team to:

* Update our README file and implement our 3rd party API to retrieve the available exercises a user can add to their workout

I began by merging my changes from last Thursday because I hadn't completed that before the end of class. Then I started by forking the example README shared by Dalonte and made changes to the badges to accurately display our tech stack. I'm having trouble getting all of them to show, so I will look into that further tomorrow. I then spent a large part of the day helping to implement the 3rd party api request and accurately map the muscle selections to make the api call while Jon coded. We got a lot done and should be ready to finish the feature tomorrow.

## February 6

Today I worked with my team to:

* Implement our 3rd party API request and return our exercise options to the front end form

Today we were able to successfully implement our 3rd party API request and get the data back. We implemented the call on the back end and used the frount end to send a request to the back end route that handles the call. I much prefer this method opposed to the front-end handling because it enforces authentication methods and keeps most of the processing behind the scenes. We had a little hiccup with the API key, but it was a silly mistake involving a misunderstand of the API documentation that we resolved pretty quickly. We still need to get the form to submit the data to create an exercise and will complete that tomorrow.

## February 7

Today I worked with my team to:

* Work on refactoring the code for better user authentication and finishing the last couple unit tests.

I refactored all of the routes to use the backend user account authentication method. That took me all morning. I also ran into an issue with the update route for our exercises. It turns out that we were not actually updating anything at all, but it still returned the data so we thought we were updating when we weren't. Bart helped us realize the problem and I was able to fix the problem. We also lost all of our third party api code due to a merging error, so Thomas had to help us retrieve an old commit to get it back. That took a lot of time, but was very necessary. We were also able to succesfully complete both of the remaining unit tests which feels great. One less thing on our to-do list.

## February 8

Today I worked with my team to:

* Finish our workout page form and connect the date selected by the user on the dashboard to the workout page form

One more day! We completed a lot today. First we worked on completing our workout page form to allow a user to submit a workout. We hardcoded a date at first because we needed the form to work before we connected it to the user home page to have the date provided dynamically by the user selection on the calendar. We finished that feature at the end of the day as well. I also figured out how to get the tech stack badges to work on the README so we should be close to finished with that as well. Tomorrow we will focus on cleaning up our code, formatting, and finishing the README.

## February 9

Today I worked with my team to:

* Submit our project

We focused on finishing our edit excercise function as well as final code clean up and merges. It was a very hectic day, but we all worked well together and are ready for whatever comes next week.
