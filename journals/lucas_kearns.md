## January 17

Today I worked with my team to:

*Complete our Postgres DB setup and begin setting up user authentication

Justin completed the setup of our project's postgres database. With that finished, I was able to begin working on our user authentication api. I followed the instructions and videos from morning lecture, as well as used the supplimentary information found on learn. While I was able to add a login/logout function using fastAPI, we could not complete.

Our plans for tomorrow are to theorycraft and figure out a method to implement a create user function using postgres.


## January 18

Today I worked with my team to:

*Complete our User Authentication and begin creating our DB table for ecercises

Justin and I finished working on the user authentication. It is now possible to create users, login, logout, and get a user token. These changes were pushed and merged onto the main branch.

With that complete, we made a new branch and began creating our DB table for exercises. Jon drove while we reviewed lessons and documentation to help troubleshoot problems. After that, we started to create a model and work on fastAPI implementation for creating new exercises, but we struggled with this, and affter an evening of invalid requests and internal server errors, we decided to wrap things up and take a fresh look at our project tomorrow.

Our plans for tomorrow are to continue developing out create exercise endpoint and request assistance if needed as to not take too much time on this component of the project.


## January 19

Today I worked with my team to:

*Complete our exercise model with fastAPI endpoints

Jon and Hector took seperate turns creating our first full model for the project. After a long day of  work, the functions we were able to add were GET, POST, PUT, and DELETE. Exercises are crucial to the functionality and after we develop our remaining models, we plan to focus on populating a list of ecercises with data from a 3rd party API.

Next week we will begin constructing our workout and user profile models.


## January 22/23

Over the course of these two days, I worked with my team to:

*Complete our database and backend

All four of us took turns over to finish up all backend functionality over the course of two days. A model for a given user's "health data" was created and given POST, PUT, and GET functionality, allowing a user to create and update their physical attributes as well as their workout goals. We plan to allow for a user to view their changes in health over time.

We decided to exclude the "workout" model from our database and backend. Instead, I drove for our team and created a workout endpoint which queries exercises using the exercise's date and user id as paramaters. Now any workout can be retrieved directly from the exercise database. This should both simplify and streamline the application, as well as making it easier to pass data to the front end.

Hector drove on the evening of the 23rd and worked on error handling. We recieved significant guidance from Bart on how to make sure we are actually catching errors and returning data properly. Python is somewhat convoluted when compared to error handling in other languages; despite this, we established a simple, effective method of raising errors with proper messages for the backend.

Tomorrow, we are excited to begin looking to the development of our frontend. We plan to use a significant amount of our time tomorrow to just do general research and rewatch/reread lectures/documentation.


## January 24

Today I worked with my team to:

*Begin setting up our frontend

Today, we spend a lot of time as a team attempting to setup Vite, and get our first few messages to appear on the frontend display of our application. Progress was slow, but we made a lot of key breakthroughs, particulairly when it came to file formatting and the differences between .js and .jsx files.

Tomorrow we plan to have a slower, more research-based day to catch up on class material and study frontend concepts.


## January 25

Today I worked with my team to:

*Research, Study material from learn, and catch up on previous class assignments

After a productive week, several of us are feeling a little stressed and decided to take a day to work on our foundational programming skills and not focus as hard on the project today. Each team member was free to choose which topics to study, and and significant questions were brought up with the group for discussion.

Some minor work was done with Justin as driver where we attempted to implement a unit test, but it was decided to wait until after lecture tomorrow before we fully dive into it.


## January 26

Today I worked with my team to:

* Begin creating function Unit tests for our project

Today, we begain looking into the creation of our unit tests for our final submission. I drove today and attempted to create a test for our create health data endpoint. This turned out to be surpurisingly difficult, as POST unit tests require more functionality than a GET request. We used video lectures and learn documentation to create the framework of our test.

Progress on our unit test was slow, as the examples both in lecture and documentation were incomplete. I spent a couple hours debugging in the evening, and even with the assistance of two SEIRs, we decided it would be best to let things sit over the weekend and come back with a fresh perspective.

Next week we plan to fully dive in to the frontend development of our project.


## January 29

Today I worked with my team to:

* Complete our first unit test and begin frontend development.

Coming back today, I was able to fix up the errors on our create health data unit test and finally get it to pass consistantly. This was a big relif to both myself and the rest of the team. I am aware we could have done a simpler test, but the satisfaction of creating a new test from scratch definitley made it worth it.

In the afternoon and evening, Jon drove while we worked on our first visible frontend outputs. We decided to start working on the login/logout/create user and hopepage sections first. We were able to get the look of the page down, but the functionality isn't quite there yet. We plan to use tomorrow to lock down our login functionality as well as our frontend authentication if possible, though we expect that we'll need some help to get it done.


## January 30

Today I worked with my team to:

* Add functionality to our front end homepage

Today, we spent most of our project time attempting to add the login, logout, and create account functions to our project frontend. This proved to be considerably difficult and we requested the assistance of SEIRs and instructors, but we now have a "mostly" functional page. We still need to add redirects and work some more on frontend authentication, but progress is progress.

As tomorrow is the last day of this school week, we plan to start out logged in homepage and at least have the outline for out remaining frontend pages before the weekend.


## January 31

Today I worked with my team to:

* add various other pages to our frontend

Today, we continued to build up our frontend by adding the remaining pages to our application including the logged in homepage, the health data page, the workout page, and touchups to our logged out user homepage. We planned for each one of us to begin adding functionality tomorrow, and use the final week of project time to complete these remaining functions.


## February 1

Today I worked with my team to:

* begin adding functionality to our frontend Health Data Page

Today, I worked on pulling data from the backend to the front of out health data page. After plenty of trial and error, I was able to sucessfully pull both the user's account data as well as their health data; furthermore, I added in a safeguard that redirects the user to the login page if there is currently not an authenticated user on the page. This functionality is crucial, as it prefents unauthorized users from modifying the data of other accounts.
=======


## February 5

Today I worked with my team to:

* Construct the frontend for a user's given workout

Jon drove today as we worked on the frontend of workouts display. This is the page that will display all of the exercises done by a single user on a specific day. This requirers us to pull both the user ID and the selected date to this page, which we found to be rather difficult, but we did eventially find a method of pulling the user data to the front end so that we can use it to query exercises. Once we can get the date as well, our project will be nearly functionally complete.


## February 6

Today I worked with my team to:

* Finish all functionality for a user's healthdata

Today, I worked on the user healthdata page and the create user healthdata page. I made it so that a user with no previously existing healthdata is automatically redirected to a new page with a form that requires the user to put theur healthdata into the database for the first time. When submitted, this form will redirect the user back to the healthdata page where their most recently updated healthdata will be displayed.

The healthdata page also now contains a button which allows the user to modify their healthdata. This will, agauin, redirect them to the healthdata form to update all their information.

With all of this functionality done, The next steps for me are to add CSS and additional formatting to give the healthdata pages a more professional and unique look.


## February 7

Today I worked with my team to:

* Reformat our backend to better authenticate our users

Today, we spent most of the day fixing errors in our frontend and unit tests. Justin fixed an issue involving User IDs and authentication on the backend, which made our application more secure; however, this also caused all calls to our FastAPI endpoints to not function properly. I first worked on getting my frontend back up and running, but this has delayed my work on the CSS frontend, which will likely just be pushed to tomorrow now.

The much more annoying issue was that this broke all our unit tests, as our new backend made it so that we were required to mock an authenticated user, which we found to be very difficuly. This was eventually resolved with much trial and error, but all functionality has been restored and we are now prepared to continue working on workout frontend functionality as well as healthdata CSS formatting.


## February 8

Today I worked with my team to:

*Workout submission with variations in date selection

Today, I finished my frontend formatting for the healthdata, but the far more significant milestone was adding the ability for our application to carry a date value between web pages. This was done with several different members swapping the current driver as we did troubleshooting and error handling. We used the assistance of Instructors and SIERs

As the project due date is tomorrow, we plan to use tomorrow to wrap up and polish our project and ensure that we're meeting all grading criteria. We plan to clean code, and remove all JS errors from on console.
